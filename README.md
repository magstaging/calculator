# README #

Implement an API point that enable the end-user to perform calculations using the Magento API interface

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/Calculator when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

If using postman:
<magento_url>/index.php/rest/V1/api/calculator
Header set to:
Content-Type: application/json
Accept: application/json

raw body: 
{
  "left":4,
  "right":5,
  "operator":"add"
}

result format:
{
    "status": "OK",
    "result": 9
}
