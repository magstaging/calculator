<?php

namespace Mbs\Calculator\Model;

use Mbs\Calculator\Api\Data\OperationResultInterface;
use Mbs\Calculator\Api\OperatorInterface;

class Operator implements OperatorInterface
{
    /**
     * @var Operator\OperandValidator
     */
    private $operandValidator;
    /**
     * @var Operator\OperatorProcessor
     */
    private $operatorProcessor;
    /**
     * @var Operator\ResultParser
     */
    private $resultParser;

    public function __construct(
        \Mbs\Calculator\Model\Operator\OperandValidator $operandValidator,
        \Mbs\Calculator\Model\Operator\OperatorProcessor $operatorProcessor,
        \Mbs\Calculator\Model\Operator\ResultParser $resultParser
    ) {
        $this->operandValidator = $operandValidator;
        $this->operatorProcessor = $operatorProcessor;
        $this->resultParser = $resultParser;
    }

    /**
     * @param float $left
     * @param float $right
     * @param string $operator
     *
     * @return OperationResultInterface
     */
    public function compute(
        float $left,
        float $right,
        string $operator
    ): OperationResultInterface {
        try {
            $this->operandValidator->validateOperator($operator);
            $result = $this->operatorProcessor->performOperation($left, $right, $operator);
            $statusCode = 'OK';
        } catch (\Mbs\Calculator\InvalidOperator $e) {
            $result = 0;
            $statusCode = 'ERROR';
        }

        return $this->resultParser->convertToResult($statusCode, $result);
    }
}
