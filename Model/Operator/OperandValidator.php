<?php

namespace Mbs\Calculator\Model\Operator;

class OperandValidator
{
    private static $availableOperator = ['add' , 'substract', 'multiply', 'divide' , 'Power'];

    public function validateOperator(string $operator)
    {
        if (!in_array($operator, self::$availableOperator)) {
            throw new \Mbs\Calculator\InvalidOperator();
        }
    }
}
