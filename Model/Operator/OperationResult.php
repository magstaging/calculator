<?php

namespace Mbs\Calculator\Model\Operator;

use Mbs\Calculator\Api\Data\OperationResultInterface;

class OperationResult implements OperationResultInterface
{
    /**
     * @var string
     */
    private $status;

    /**
     * @var float
     */
    private $result;

    /**
     * OperationResult constructor.
     * @param string $status
     * @param float $result
     */
    public function __construct(
        string $status,
        float $result
    ) {
        $this->status = $status;
        $this->result = $result;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return float
     */
    public function getResult(): float
    {
        return $this->result;
    }
}
