<?php

namespace Mbs\Calculator\Model\Operator;

class OperatorProcessor
{
    public function performOperation(float $left, float $right, string $operator)
    {
        switch ($operator) {
            case 'add':
                $result = $left + $right;
                break;
            case 'substract':
                $result = $left - $right;
                break;
            case 'multiply':
                $result = $left * $right;
                break;
            case 'divide':
                $result = $left / $right;
                break;
            case 'Power':
                $result = pow($left, $right);
                break;
            default:
                throw new \Mbs\Calculator\InvalidOperator();
        }

        return $result;
    }
}
