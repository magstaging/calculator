<?php

namespace Mbs\Calculator\Model\Operator;

use Mbs\Calculator\Api\Data\OperationResultInterfaceFactory;

class ResultParser
{
    /**
     * @var OperationResultInterfaceFactory
     */
    private $operationResultFactory;

    /**
     * resultParser constructor.
     *
     * @param OperationResultInterfaceFactory $operationResultFactory
     */
    public function __construct(
        OperationResultInterfaceFactory $operationResultFactory
    ) {
        $this->operationResultFactory = $operationResultFactory;
    }

    /**
     * @param string $statusCode
     * @param int $result
     * @return \Mbs\Calculator\Api\Data\OperationResultInterface
     */
    public function convertToResult(string $statusCode, int $result)
    {
        $operationResult = $this->operationResultFactory->create([
            'status' => $statusCode,
            'result' => $result
        ]);

        return $operationResult;
    }
}
