<?php

namespace Mbs\Calculator\Api;

use Mbs\Calculator\Api\Data\OperationResultInterface;

interface OperatorInterface
{
    /**
     * @param float $left
     * @param float $right
     * @param string $operator
     *
     * @return OperationResultInterface
     */
    public function compute(
        float $left,
        float $right,
        string $operator
    ): OperationResultInterface;
}
