<?php

namespace Mbs\Calculator\Api\Data;

interface OperationResultInterface
{
    /**
     * @return string
     **/
    public function getStatus();

    /**
     * @return float
     **/
    public function getResult();
}
